# MEP: Support Search by ID

Current state: Under Discussion

ISSUE: [#5215 Support using the ID search that already exists in the collection](https://github.com/milvus-io/milvus/issues/5215)

PRs:

Keywords:

Released:

## Summary

When the vector is stored in Milvus, the corresponding ID will be generated. If the vector to be queried already exists, it will be more efficient to query directly by ID. However, the current version does not support querying by ID. We can only use the ID to retrieve the vector and then use the vector for query, which reduces the efficiency.

This proposal is about

- deciding the API of the new function
- The details on how to support search by ID

## Motivation

1. **Complicated Operation**: Without the new function, users need to call the get_entity_by_id() and search() to perform vector querying by id 
2. **Low querying efficiency**: Calling functions get_entity_by_id() and search() both need to load data into memory, which consume time

## Design Details

 ### A. What is the API of the new function?

 **Plan** (Recommended) : The API of the new function is the same as the search() function, except for one parameter: query_record is replaced by query_id

   **Pros:** 

​   **Cons:** Not see any.

 ### B. How to search by ID?

 **Plan B1** : Directly use the existing functions search() and get_entity_by_id() to construct new function

​	**Pros:** Ruduce operations in querying by id. Only need to call the function once. 

​	**Cons:** The efficiency of the querying behaviour is still low and has not changed


 **Plan B2** (Recommended): The new function will :
                                1 Read the data required by the vector querying into the memory

                                2 Find the vector corresponding to id from the data in the memory

                                3 Use the vector to do vector querying with the data that has been read in the memory

​	**Pros:**

​			1 Ruduce operations in querying by id. Only need to call the function once.

​			2 Impoving the dfficiency of the querying behaviour by avoiding duplicated data loading.

​	**Cons:** Not see any


## Compatibility, Deprecation, and Migration Plan

Need to merge the new function into 2 python SDKs for Milvus, PyMilvus and PyMilvus-ORM


## Test Plan

- need to pass the unittest
- need to pass the basic python test case
